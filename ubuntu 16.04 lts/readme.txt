README: NMLSim - Nanomagnetic Simulator
Release 1.0
Last modification: 19/06/2017

NMLSim is Nanomagnetic Logic Simulator tool developed at Nanocomp/DCC/UFMG.

This is a stable version of our NML Simulator (NMLSim). Currently the tool works perfectly only on Linux platforms. The tool have a simulation algorithm developed in C++ and an interface developed in Java.

If Java Package isn't installed in your system or the version is lesser than 8, try to install with the commands below:
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

Command to run the interface: java -jar nmlsim.jar



